package it.uniroma3.siw.taskmanager.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public User getUser(Long id) {
		Optional<User> result = userRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public User getUser(String firstName) {
		Optional<User> result = userRepository.findByfirstName(firstName);
		return result.orElse(null);
 	}
	
	@Transactional
	public User saveUser(User user) {
		return this.userRepository.save(user);
	}
	
	@Transactional
	public List<User> findAllUser() {
		List<User> lista = new ArrayList<>();
		Iterable<User> iterable =  this.userRepository.findAll();
		for(User u : iterable) {
			lista.add(u);
		}
		return lista;
	}
	
	@Transactional
	public List<User> getMembers(Project project){
		List<User> listaMembers = new ArrayList<>();
		Iterable<User> iterable = this.userRepository.findByVisibleProjects(project);
		for(User u : iterable) {
			listaMembers.add(u);
		}
		return listaMembers;
	}
	
	
}

