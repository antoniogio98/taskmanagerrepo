package it.uniroma3.siw.taskmanager.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.Tag;
import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.repository.ProjectRepository;

@Service
public class ProjectService {

	@Autowired
	public ProjectRepository projectRepository;
	
	@Transactional
	public Project getProject(Long id) {
		Optional<Project> result = this.projectRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public Project saveProject(Project project) {
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public void deleteProject(Project project) {
		this.projectRepository.delete(project);
	}
	
	@Transactional
	public void deleteProject(Long id) {
		this.projectRepository.deleteById(id);
	}
	
	@Transactional
	public void deleteProject(String name) {
		this.projectRepository.deleteByName(name);
	}

	@Transactional
	public Project addTask(Project project, Task task) {   
		project.addTask(task);
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public Project addTag(Project project, Tag tag) {   
		project.addTag(tag);
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public Project shareProjectWithUser(Project project, User user) {
		project.addMember(user);
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public List<Project> retrieveProjectsOwnedBy(User user) {    
		List<Project> projectsList = new ArrayList<>();
		Iterable<Project> iterable = this.projectRepository.findByOwner(user);
		for(Project p : iterable) {
			projectsList.add(p);
		}	
		return projectsList;
	}
	
	@Transactional
	public List<Project> retrieveProjectsShered(User member) {    
		List<Project> projectsSharedList = new ArrayList<>();
		Iterable<Project> iterable = this.projectRepository.findByMembers(member);
		for(Project p : iterable) {
			projectsSharedList.add(p);
		}
		return projectsSharedList;
	}
	
	
	
	
}
