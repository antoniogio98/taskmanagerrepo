package it.uniroma3.siw.taskmanager.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.taskmanager.model.Comment;
import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.repository.CommentRepository;


@Service
public class CommentService {

	@Autowired
	public CommentRepository commentRepository;
	
	
	@Transactional
	public Comment saveComment(Comment comment) {
		return this.commentRepository.save(comment);
	}
	


	@Transactional
	public List<Comment> retrieveComments(Task task) {    
		List<Comment> commentList = new ArrayList<>();
		Iterable<Comment> iterable = this.commentRepository.findByTask(task);
		for(Comment c : iterable) {
			commentList.add(c);
		}
		return commentList;
	}
	

	
	
}
