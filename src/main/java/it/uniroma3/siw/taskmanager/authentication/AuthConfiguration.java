package it.uniroma3.siw.taskmanager.authentication;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity

/* Classe di configurazione
 * WebSecurityConfigurerAdapter: è un oggetto di Spring Security
 * */
public class AuthConfiguration extends WebSecurityConfigurerAdapter{
	public static final String ADMIN_ROLE = "ADMIN";   
	
	@Autowired
	DataSource dataSource;   //componenet di Spring che odella un punto di accesso al DB
	
	
	
	/* qui scriviamo le politiche effettive di autenticazione e autorizzazione
	 * una lunga serie di invocazioni "concatenate" all'oggetto http
	 * i metodi di http di solito restituisconolo stesso oggetto http(aggiornato), in modo da poter concatenare le chiamate 
	 * */
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http   // sezioni di concatenazioni
		
		//paragrafo dell'autorizzazione : qui definiamo chi puo accedere a quali pagine--> autorizzazione,no autenticazione
		.authorizeRequests()
		.antMatchers(HttpMethod.GET, "/", "/index", "/login", "/users/register", "/css/**", "/images/**" ).permitAll() //chiunque puo manadre richieste GET a url : / ,index..
		.antMatchers(HttpMethod.POST, "/login", "/users/register" ).permitAll()             //chiunque puo manadre richieste POST a url :..
	
		.antMatchers(HttpMethod.GET, "/admin/**").hasAnyAuthority(ADMIN_ROLE)     // la puoi visualizzare solo se sei admin
		.antMatchers(HttpMethod.POST, "/admin/**").hasAnyAuthority(ADMIN_ROLE)   //**->qualsiasi URL che inizia con /admin/...puo essere acceduto solo da admin
		
		.anyRequest().authenticated()        // qualsiasi altra richiesta solo se l'utente è autenticato, (sia admin che default)
	
		/* LOGIN: url di default :"/login"
		 * GET: per ottenere la vista del form 
		 * POST: per autenticarsi
		 * */
		.and().formLogin()
		.defaultSuccessUrl("/home")   //login con successo
	
		/* LOGOUT
		 */
		.and().logout()
		.logoutUrl("/logout")    //Get a "/logout" per uscire
		.logoutSuccessUrl("/index")
		
		/* per interrompere la sessione nel momento in cui viene fatto logout
		 */
		.invalidateHttpSession(true) 
		.clearAuthentication(true).permitAll();
	}
	
	/*  specifichiamo dove spring security deve andare a recuperare le credenziali all'interno del DB per confrontarle con quelle inviate dall'utente
	 * */
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception{
		auth.jdbcAuthentication()
		
		.dataSource(this.dataSource) //ANDPOINT per il nostro DB, dove cercare i dati
		
		//user_name --> nome della tabella in DB --> perche ho dichiarato userName
		//WHERE user_name =? --> lo userName è un parametro ecco perche c'e --> '?'
		.authoritiesByUsernameQuery("SELECT user_name, role FROM credentials WHERE user_name =?") //ottenere username e ruoli associati all'username
		
		//1 as enable --> restituisci sempre 1 perche sono sempre abilitati per noi
		.usersByUsernameQuery("SELECT user_name, password, 1 as enable FROM credentials WHERE user_name =?"); // //ottenere username e password e 1 flag booleano per sapere se l'utente è abilitato oppure no (sempre abilitati)
		
	}
	
	
	/* Per cifrare la password nel database
	 * BEAN --> l'oggetto restituito dal metodo viene salvato nel contesto dell'applicazione in modo che quando 
	 *          lo andiamo a richiamare l'app non ha bisogno di rinizializzare quell'oggetto ma usa la stessa istanza
	 *          lo possiamo ottenere con @Autpowired
	 *          --> e un tipo particolaree di @Component
	 */
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();  //al login l'app cifra la password inviata dall'utente prima di confrontarla con le credenziali salvate nel DB
	}
}
