package it.uniroma3.siw.taskmanager.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.controller.validation.TagValidator;
import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.Tag;
import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.services.ProjectService;
import it.uniroma3.siw.taskmanager.services.TagService;
import it.uniroma3.siw.taskmanager.services.TaskService;

@Controller
public class TagController {

	
	@Autowired
	TagService  tagService ;
	
	@Autowired 
	TagValidator tagValidation;
	
	@Autowired
	ProjectService  projectService ;
	
	@Autowired
	TaskService  taskService ;
	
	@Autowired 
	SessionData sessionData;


	@RequestMapping(value= { "/projects/{projectId}/{taskId}/tag" }, method = RequestMethod.GET)  //l utente lo devo cerare?
    public String createTaskForm(Model model ,@PathVariable Long projectId, @PathVariable Long taskId) {
		User loggedUser = sessionData.getLoggedUser();
		Project project = projectService.getProject(projectId);
	    Task task =  taskService.getTask(taskId);
		model.addAttribute("task", task);
		model.addAttribute("user", loggedUser);
		model.addAttribute("project", project);
		model.addAttribute("tagForm", new Tag());
		return "addTag";
	}
	
	
	
	
	@RequestMapping(value= { "/projects/{projectId}/{taskId}/tag" }, method = RequestMethod.POST) 
    public String createTask(@Valid @ModelAttribute("tagForm") Tag tag, BindingResult tagBindingResult, 
    		                 @PathVariable Long projectId,  @PathVariable Long taskId, Model model) {
	   	User loggedUser = sessionData.getLoggedUser();
	  	Project project = projectService.getProject(projectId);
	    Task task = taskService.getTask(taskId);;
			
	        //valido i campi arrivati 
	  		this.tagValidation.validate(tag, tagBindingResult);
	  		if(project.getOwner()!=loggedUser) {      //controlla bene se funziona
	  			if(!tagBindingResult.hasErrors()) {     
	  			
	  			tagService.saveTag(tag);
	  			taskService.addTag(task, tag);     
	  			projectService.addTag(project, tag);
	  			
	  			
	  			model.addAttribute("tag", tag);
	  			model.addAttribute("project", project);
	  			model.addAttribute("task", task);
	  			return "redirect:/tasks/"+ taskId;
	  		}
	  		}
	  		model.addAttribute("task", task);
	  		model.addAttribute("tag", tag);
	  		model.addAttribute("project", project);
	    	model.addAttribute("user", loggedUser);    
	    	
	  	return "addTag";  		
	}
	
	
	
	
	
}
