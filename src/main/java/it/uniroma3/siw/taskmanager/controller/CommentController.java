package it.uniroma3.siw.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.model.Comment;

import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.services.CommentService;

import it.uniroma3.siw.taskmanager.services.TaskService;

@Controller
public class CommentController {

	@Autowired 
	SessionData sessionData;
	
	@Autowired
	CommentService  commentService ;
	
	@Autowired
	TaskService  taskService ;
	

	@RequestMapping(value = { "/tasks/{taskId}/comment" }, method = RequestMethod.POST)
    public String addComment(@RequestParam("casellaCommenti") String casellaCommenti, Model model, @PathVariable Long taskId) {
		User loggedUser = sessionData.getLoggedUser();
		Task task = this.taskService.getTask(taskId);
	
		if(casellaCommenti!=null && !casellaCommenti.trim().isEmpty()) {
    		
	     	Comment comment = new Comment();
	     	comment.setUser(loggedUser);
			comment.setText(casellaCommenti);
			commentService.saveComment(comment);
			taskService.addComment(task, comment);
			
			model.addAttribute(casellaCommenti, casellaCommenti);
			return "redirect:/tasks/" + taskId;
    	}
	
		return "redirect:/home";  		
    }
	
	



	
}
