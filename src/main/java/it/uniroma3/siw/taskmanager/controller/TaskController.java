package it.uniroma3.siw.taskmanager.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.controller.validation.TaskValidator;
import it.uniroma3.siw.taskmanager.model.Comment;
import it.uniroma3.siw.taskmanager.model.Credentials;
import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.Tag;
import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.services.CommentService;
import it.uniroma3.siw.taskmanager.services.CredentialsService;
import it.uniroma3.siw.taskmanager.services.ProjectService;
import it.uniroma3.siw.taskmanager.services.TaskService;
import it.uniroma3.siw.taskmanager.services.UserService;


@Controller
public class TaskController {
	
	
	@Autowired
	TaskService  taskService ;
	
	@Autowired
	ProjectService  projectService ;
	
	@Autowired 
	UserService userService;
	
	@Autowired
	TaskValidator taskValidator;
	
	@Autowired 
	SessionData sessionData;
	
	@Autowired
    CredentialsService credentialsService;
	
	@Autowired
    CommentService commentService;
	
	
	@RequestMapping(value = { "/tasks/{id}" }, method = RequestMethod.GET)
	public String task(Model model, @PathVariable Long id ) {
		User loggedUser = sessionData.getLoggedUser();
		Task task = taskService.getTask(id);
		
		if(task==null)                     
		   return "redirect:/projects";
		
		List<Tag> tags = task.getTags();
	    List<Comment> comments = commentService.retrieveComments(task);
	
	 	model.addAttribute("comments", comments);
		model.addAttribute("tags", tags);
		model.addAttribute("task", task);
		model.addAttribute("user", loggedUser);
		return "task";  
	}
	
	
		@RequestMapping(value = { "/tasks/{taskId}/delete" }, method = RequestMethod.POST)
	    public String removerProject(Model model, @PathVariable Long taskId) {
	    	this.taskService.deleteTask(taskId);
	    	 return "redirect:/projects";  
	    }
		
		

		
@RequestMapping(value= { "/projects/{projectId}/task" }, method = RequestMethod.GET)  //l utente lo devo cerare?
    public String createTaskForm(Model model ,@PathVariable Long projectId) {
		User loggedUser = sessionData.getLoggedUser();
		Project project = projectService.getProject(projectId);
	
		
		model.addAttribute("user", loggedUser);
		model.addAttribute("project", project);
		model.addAttribute("taskForm", new Task());
		return "addTask";
	}
	
	@RequestMapping(value= { "/projects/{projectId}/task" }, method = RequestMethod.POST) 
    public String createTask(@Valid @ModelAttribute("taskForm") Task task, BindingResult taskBindingResult, 
    		                 @PathVariable Long projectId, Model model) {
	   	User loggedUser = sessionData.getLoggedUser();
	   	Project project = projectService.getProject(projectId);
	 
			
	  //valido i campi arrivati 
	  		this.taskValidator.validate(task, taskBindingResult);
	  		if(!taskBindingResult.hasErrors()) {  
	  			task.setCompleted(false);
	  			project.addTask(task);
	  			this.projectService.saveProject(project);
	  			return "redirect:/projects/"+ projectId;
	  		}
	  
		model.addAttribute("user", loggedUser);    //con errori mostro di nuovo il form con i campi errore
		return "addTask";
	}
	

  	@RequestMapping(value= { "/projects/{projectId}/{taskId}/update" }, method = RequestMethod.GET) 
      public String updateTaskForm(Model model, @PathVariable Long projectId, @PathVariable Long taskId) {
  		User loggedUser = sessionData.getLoggedUser();
  		Project project = projectService.getProject(projectId);
        Task task = taskService.getTask(taskId);
        
  		model.addAttribute("project", project);
  		model.addAttribute("user", loggedUser);  
  		model.addAttribute("task", task);
  		model.addAttribute("taskForm", new Task());
  		return "updateTask";
  	}


  	
  	
  	@RequestMapping(value= { "/projects/{projectId}/{taskId}/update" }, method = RequestMethod.POST) 
      public String updateTask( @Valid @ModelAttribute("taskForm") Task taskForm,
             BindingResult taskBindingResult, @PathVariable Long projectId, @PathVariable Long taskId, Model model) {
  		
  			User loggedUser = sessionData.getLoggedUser();   
  			Project project = projectService.getProject(projectId);
  		 
  			this.taskValidator.validate(taskForm, taskBindingResult);
  			
  			if(!taskBindingResult.hasErrors()) {
  				Task taskOld = taskService.getTask(taskId);
  			
  				taskOld.setName(taskForm.getName());
  				taskOld.setDescription(taskForm.getDescription());
  				this.projectService.saveProject(project);
  				this.taskService.saveTask(taskOld);
  				model.addAttribute("user", loggedUser);    
          return "redirect:/tasks/" + taskOld.getId(); 
      }
     
      	
  		
  		return "updateTask";
  	}
	
    
	@RequestMapping(value= { "/projects/{projectId}/{taskId}/assigns" }, method = RequestMethod.GET)      
    public String createAssignsForm( @PathVariable Long projectId ,  @PathVariable Long taskId, Model model) {
		User loggedUser = sessionData.getLoggedUser(); 
		Project project = this.projectService.getProject(projectId);
		Task task = this.taskService.getTask(taskId);
		
		model.addAttribute("user", loggedUser);
		model.addAttribute("project", project);
		model.addAttribute("task", task);
		return "assignsTaskTo";
	}

	@RequestMapping(value= { "/projects/{projectId}/{taskId}/assigns" }, method = RequestMethod.POST) 
    public String assignsTask(@RequestParam("username") String username, @PathVariable Long projectId, @PathVariable Long taskId, Model model) {
		User loggedUser = sessionData.getLoggedUser(); 
		Project project = this.projectService.getProject(projectId);
		Task task = this.taskService.getTask(taskId);
		
	   	Credentials credentials = this.credentialsService.getCredentials(username); 
	  if(credentials==null) {
		  model.addAttribute("user", loggedUser);
	   	 return "redirect:/home";  		
	  }
	  
	  
     	User membro = credentials.getUser();
	   	
     	
    	//il task è assegnato ad un solo utente tra quelli che hanno visibilità del progetto
     	List<User> members = project.getMembres();
	   
	   	if(!members.contains(membro)) {  //l'untente non ha la visibilita
	   		model.addAttribute("user", loggedUser);
		   	return "redirect:/home";  		
	   	
	       }
	    else {
	   			task.setUser(membro);
	   			this.taskService.saveTask(task);
	   			this.projectService.saveProject(project);
	   		  	model.addAttribute("user", loggedUser);
	   			return "redirect:/tasks/" + task.getId();       
	   		}
	   	}
	   



}

