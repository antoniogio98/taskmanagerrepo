package it.uniroma3.siw.taskmanager.controller;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.controller.validation.CredentialsValidator;
import it.uniroma3.siw.taskmanager.controller.validation.UserValidator;
import it.uniroma3.siw.taskmanager.model.Credentials;
import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.repository.UserRepository;
import it.uniroma3.siw.taskmanager.services.CredentialsService;
import it.uniroma3.siw.taskmanager.services.ProjectService;
import it.uniroma3.siw.taskmanager.services.UserService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * The UserController handles all interactions involving User data.
 */
@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserValidator userValidator;

    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Autowired
    CredentialsService credentialsService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    CredentialsValidator credentialsValidator;
    
    @Autowired
    ProjectService projectService;
    

    /* serve per farci restituire i dati dell'utente e le credenziali
     * ogni volta che al controller serve sapere quale è l'utente autenticato minimizzando l'accesso al DB
     */
    @Autowired
    SessionData sessionData;
   
    @RequestMapping(value = { "/home" }, method = RequestMethod.GET)
    public String home(Model model) {
    	User loggedUser = sessionData.getLoggedUser();    
    	model.addAttribute("user", loggedUser);       //inserisci il nome dell'utente autenticato nella vista--> "Benvenuta, Alessia"
        return "home";
    }
    
   
    @RequestMapping(value = { "/users/me" }, method = RequestMethod.GET)
    public String me(Model model) {
    	User loggedUser = sessionData.getLoggedUser();  
    	Credentials credentials = sessionData.getLoggedCredentials();
    	System.out.println(credentials.getPassword());
    	model.addAttribute("user", loggedUser);     
    	model.addAttribute("credentials", credentials); 
        return "userProfile";
    }
    

    @RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
    public String admin(Model model) {
    	User loggedUser = sessionData.getLoggedUser();    
    	model.addAttribute("user", loggedUser);       
        return "admin";
    }

    @RequestMapping(value = { "/admin/users" }, method = RequestMethod.GET)
    public String usersList(Model model) {
    	User loggedUser = sessionData.getLoggedUser();
    	List<Credentials> allCredentials = credentialsService.getAllCredentials();
    	model.addAttribute("user", loggedUser);
    	model.addAttribute("credentialsList", allCredentials);
    	return "allUsers";
    }
    
    @RequestMapping(value = { "/admin/users/{username}/delete" }, method = RequestMethod.POST)
    public String removerUser(Model model, @PathVariable String username) {
    	this.credentialsService.deleteCredentials(username);
    	 return "redirect:/admin/users";   
    }
    
    
    
    @RequestMapping(value = { "/admin/users/{username}/projects" }, method = RequestMethod.GET)
    public String projectsUser(Model model, @PathVariable String username) {
    	User loggedUser = sessionData.getLoggedUser();
    	Credentials credentials = this.credentialsService.getCredentials(username);
    	User user = credentials.getUser();
    	List<Project> projectsList = projectService.retrieveProjectsOwnedBy(user);
    	model.addAttribute("projectsList", projectsList);
    	model.addAttribute("username", username);
    	model.addAttribute("user", loggedUser);
    	 return "projectDelete";   
    }
    
    

 @RequestMapping(value = { "/admin/users/{username}/{nameProject}/delete" }, method = RequestMethod.POST)
    public String removerProject(Model model, @PathVariable String nameProject,  String username ) {
    	this.projectService.deleteProject(nameProject);
    	 return "redirect:/admin/users"; 
    }

    
 
 
  	@RequestMapping(value= { "/user/update" }, method = RequestMethod.GET) 
      public String updateUserForm(Model model) {
  		User loggedUser = sessionData.getLoggedUser();
  		Credentials credentials = sessionData.getLoggedCredentials();
  		
  		model.addAttribute("credentials", credentials);
  		model.addAttribute("user", loggedUser);  
  		model.addAttribute("userForm", new User()); 
  		return "updateProfile";
  	}


  
  	@RequestMapping(value= { "/user/update" }, method = RequestMethod.POST) 
      public String updateUser( @Valid @ModelAttribute("userForm") User user,
              BindingResult userBindingResult, Model model) {
  	          User loggedUser = sessionData.getLoggedUser();   //vecchio
  		      this.userValidator.validate(user, userBindingResult);
      
  		     
  		     if(!userBindingResult.hasErrors()) {
  		    	 user.setId(loggedUser.getId());
  		    	 this.userService.saveUser(user);
  		    	 this.sessionData.update();
  		    	 return "updateSuccessfull";
  		     }
     
  		model.addAttribute("user", loggedUser);    
  		return "updateProfile";
  	}
    
    
    
    
    
   
}
