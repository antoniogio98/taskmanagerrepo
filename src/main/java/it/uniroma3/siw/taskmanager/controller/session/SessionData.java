package it.uniroma3.siw.taskmanager.controller.session;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import it.uniroma3.siw.taskmanager.model.Credentials;

import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.repository.CredentialsRepository;



/* Scope -> questo oggetto non vive come normalmente farebbero gli oggetti Component per la durata dell'intera vita dell'app,
 * questo va istanziato separatamente in ogni sessione che ogni utente instaura con la nostra app
 * visuale limitata alla singola sessione
 */

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)

public class SessionData {

	private User user;  //utenteLoggato in sessione
	
	private Credentials credentials;
	
	@Autowired
	private CredentialsRepository credentialsRepository;
	
	
	
	//controllo che l'utente non sia gia loggato
	public Credentials getLoggedCredentials() {
		if(this.credentials==null)      
			this.update();                  //cerco i dati nel DB
		return this.credentials;        
	}
	
	public User getLoggedUser() {
		if(this.user==null)
			this.update();
		return this.user;
	}
	
	
	
	/* UserDetails ha i campi: username ,password(protected) e e una lista di oggetti GrantedAuthority
	 * Ogni GrantedAuthority contiene una stringa che rappresenta un ruolo coperto dall'utente autenticato (admin o default)
	 * è simile a credentials ma non è sufficiente solo lo UserDetails perche non ha l'id e lo User dello credentials
	 * usiamo lo UserDetails per recuperare Credentials, tramite l'username dal DB
	 * --> retrive by username di Credentials
	 * */

	public void update() {      //prendo i campi dal DB
        UserDetails loggedUserDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        this.credentials = this.credentialsRepository.findByUserName(loggedUserDetails.getUsername()).get();//loggedUserDetails prende l'username, va dal credentialsRepository e si fa restituire le credenziali per quel username
        this.credentials.setPassword("[PROTECTED]");  //setta la password per motivi di sicurezza
        this.user = this.credentials.getUser();      //setta l'user con le credenziali recuperate
    	
    }

	
	
}
