package it.uniroma3.siw.taskmanager.controller.validation;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.taskmanager.model.Credentials;
import it.uniroma3.siw.taskmanager.services.CredentialsService;

@Component
public class CredentialsValidator implements Validator{
	
	@Autowired
	CredentialsService credentialsService; 
	
	final Integer MAX_USERNAME_LENGHT = 20;
	final Integer MIN_USERNAME_LENGHT = 4;
	final Integer MAX_PASSWORD_LENGHT = 20;
	final Integer MIN_PASSWORD_LENGHT = 6;
	

	@Override
	public void validate(Object o, Errors errors) {
		Credentials credentials = (Credentials) o;
		String userName = credentials.getUserName().trim();
		String password = credentials.getPassword().trim();
		
		
		if(userName.trim().isEmpty())                        //se il campo è vuoto
			errors.rejectValue("userName", "required");
		 else if(userName.length() <  MIN_USERNAME_LENGHT || userName.length() > MAX_USERNAME_LENGHT) 
			errors.rejectValue("userName", "size");
		 else if(this.credentialsService.getCredentials(userName)!=null)    //controllo che non sia già presente questo username
			 errors.rejectValue("userName", "duplicate");
		
		
		if(password.trim().isEmpty()) 
			errors.rejectValue("password", "required");
		 else if(password.length() < MIN_PASSWORD_LENGHT || password.length() > MAX_PASSWORD_LENGHT) 
			errors.rejectValue("password", "size");
		
	}
	
	/*//per validare uno username dal form Share
	public void validateUsername(Object o, Errors errors) {
		Credentials credentials = (Credentials) o;
		String userName = credentials.getUserName().trim();
		
		if(userName.trim().isEmpty())                        //se il campo è vuoto
			errors.rejectValue("userName", "required");
		 else if(userName.length() <  MIN_USERNAME_LENGHT || userName.length() > MAX_USERNAME_LENGHT) 
			errors.rejectValue("userName", "size");
	
		if(this.credentialsService.getCredentials(userName)==null)       //controllo che sia già presente questo username
	   			errors.rejectValue("userName", "notExist");
	   			
	   	
	}
	*/
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Credentials.class.equals(clazz);
	}
}
