package it.uniroma3.siw.taskmanager.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.taskmanager.model.User;

@Component
public class UserValidator implements Validator{
	
	final Integer MAX_NAME_LENGHT = 100;
	final Integer MIN_NAME_LENGHT = 2;
	

	@Override
	public void validate(Object o, Errors errors) {
		User user = (User) o;
		String firstName = user.getFirstName().trim();
		String lastName = user.getLastName().trim();
		
		if(firstName.trim().isEmpty())  //se il campo è vuoto
			errors.rejectValue("firstName", "required");
		 else if(firstName.length() < MIN_NAME_LENGHT || firstName.length() > MAX_NAME_LENGHT) 
			errors.rejectValue("firstName", "size");
		
		
		if(lastName.trim().isEmpty()) 
			errors.rejectValue("lastName", "required");
		 else if(lastName.length() < MIN_NAME_LENGHT || lastName.length() > MAX_NAME_LENGHT) 
			errors.rejectValue("lastName", "size");
	}
	
	
	@Override
	public boolean supports(Class<?> clazz) {  //UserValidator supporta la classe User
		return User.class.equals(clazz);
	}

	

}
