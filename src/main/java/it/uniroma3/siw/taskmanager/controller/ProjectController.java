package it.uniroma3.siw.taskmanager.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.controller.validation.CredentialsValidator;
import it.uniroma3.siw.taskmanager.controller.validation.ProjectValidator;

import it.uniroma3.siw.taskmanager.model.Credentials;
import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.services.CredentialsService;
import it.uniroma3.siw.taskmanager.services.ProjectService;
import it.uniroma3.siw.taskmanager.services.UserService;

@Controller
public class ProjectController {

	@Autowired
	ProjectService  projectService ;
	
	@Autowired 
	UserService userService;
	
	@Autowired
	ProjectValidator projectValidator;
	
	@Autowired 
	SessionData sessionData;
	
	@Autowired
    CredentialsService credentialsService;
	
	 @Autowired
	 CredentialsValidator credentialsValidator;

	 
	@RequestMapping(value = { "/projects" }, method = RequestMethod.GET)
	public String myOwnedProjects(Model model) {
		sessionData.update();
		User loggedUser = sessionData.getLoggedUser();
	    List<Project> projectsList = projectService.retrieveProjectsOwnedBy(loggedUser);  
		model.addAttribute("user", loggedUser);
		model.addAttribute("projectsList", projectsList);    
		return "myOwnedProjects";
	}
	
	
	
	@RequestMapping(value = { "/shared" }, method = RequestMethod.GET)
	public String mySharedProjects(Model model) {
		User loggedUser = sessionData.getLoggedUser();
	    List<Project> projectsSharedList = projectService.retrieveProjectsShered(loggedUser) ; 
		model.addAttribute("user", loggedUser);
		model.addAttribute("projectsSharedList", projectsSharedList);     
		return "mySharedProjects";
	}
	
	@RequestMapping(value = { "/projects/{projectId}" }, method = RequestMethod.GET)
	public String project(Model model, @PathVariable Long projectId ) {
		User loggedUser = sessionData.getLoggedUser();
		Project project = this.projectService.getProject(projectId);

		
		if(project==null)                      //controllo che il progetto esista nel DB
			return "redirect:/projects";     
		
		List<User> members = userService.getMembers(project); 
		List<Task> tasks = project.getTasks();
		if(!project.getOwner().equals(loggedUser) && !members.contains(loggedUser)) 
				return "redirect:/projects";
		
		model.addAttribute("user", loggedUser);
		model.addAttribute("project", project);
		model.addAttribute("members", members);
		model.addAttribute("tasks", tasks);
		return "project";
	}
	

	@RequestMapping(value= { "/projects/add" }, method = RequestMethod.GET) 
    public String createProjectForm(Model model ) {
		User loggedUser = sessionData.getLoggedUser();
		model.addAttribute("user", loggedUser);
		model.addAttribute("projectForm", new Project());
		return "addProject";
	}


	
	
	@RequestMapping(value= { "/projects/add" }, method = RequestMethod.POST) 
    public String createProject(@Valid @ModelAttribute("projectForm") Project project, BindingResult projectBindingResult, Model model) {
	   	User loggedUser = sessionData.getLoggedUser();
		
		//valido i campi arrivati 
		this.projectValidator.validate(project, projectBindingResult);
		if(!projectBindingResult.hasErrors()) {  
			project.setOwner(loggedUser);
			this.projectService.saveProject(project);
			return "redirect:/projects/" + project.getId();
		}
		
		
		return "addProject";   
	}
	
	
	
	@RequestMapping(value = { "/projects/{projectId}/delete" }, method = RequestMethod.POST)
    public String removerProject(Model model, @PathVariable Long projectId) {
    	this.projectService.deleteProject(projectId);
    	 return "redirect:/projects";  
    }
	
	
	
	@RequestMapping(value= { "/projects/{projectId}/update" }, method = RequestMethod.GET) 
    public String updateProjectForm(Model model, @PathVariable Long projectId) {
		User loggedUser = sessionData.getLoggedUser();
		Project project = projectService.getProject(projectId);

		model.addAttribute("project", project);
		model.addAttribute("user", loggedUser);  //utente che modifica
		model.addAttribute("projectForm", new Project());
		return "updateProject";
	}
	
	@RequestMapping(value= { "/projects/{projectId}/update" }, method = RequestMethod.POST) 
    public String updateProject( @Valid @ModelAttribute("projectForm") Project projectForm,
           BindingResult projectBindingResult, @PathVariable Long projectId, Model model) {
			User loggedUser = sessionData.getLoggedUser();   
		
			this.projectValidator.validate(projectForm, projectBindingResult);
			if(!projectBindingResult.hasErrors()) {
				Project projectOld = projectService.getProject(projectId);
				projectOld.setName(projectForm.getName());
				projectOld.setDescription(projectForm.getDescription());
				
				this.projectService.saveProject(projectOld);
				model.addAttribute("user", loggedUser);    
        return "redirect:/projects/" + projectOld.getId();
    }
		model.addAttribute("user", loggedUser); 
		return "updateProject";
	}

	
	
	@RequestMapping(value= { "/projects/{projectId}/share" }, method = RequestMethod.GET)  
    public String createShareForm(Model model, @PathVariable Long projectId) {
		User loggedUser = sessionData.getLoggedUser(); 
		Project project = this.projectService.getProject(projectId);
		
		model.addAttribute("user", loggedUser);
		model.addAttribute("project", project);
		return "shareProjectWith";
	}

	@RequestMapping(value= { "/projects/{projectId}/share" }, method = RequestMethod.POST) 
    public String shareProject(@RequestParam("username") String username, @PathVariable Long projectId, Model model) {
		User loggedUser = sessionData.getLoggedUser(); 
		Project project = this.projectService.getProject(projectId);
	   	Credentials credentials = this.credentialsService.getCredentials(username);  //ho tutte le credenziali e mi faccio restituire quelle dello user
	 
	   	if(credentials!=null) {
	   	 	 this.projectService.shareProjectWithUser(project, credentials.getUser()); 
	      	 return "redirect:/projects/" + project.getId();   
	   	
	   	}
	
	   	model.addAttribute("user", loggedUser);
	   	return "redirect:/home";  		
	}
	
}
  	
  	
  	








