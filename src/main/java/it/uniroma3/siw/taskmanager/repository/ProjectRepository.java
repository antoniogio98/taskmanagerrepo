package it.uniroma3.siw.taskmanager.repository;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.taskmanager.model.Project;

import it.uniroma3.siw.taskmanager.model.User;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long>{

	
	public List<Project> findByMembers(User member); //lista di progetti condivisi con me
	
	public List<Project> findByOwner(User owner);  //lista dei miei progetti

	public void deleteById(Long id);

	public void deleteByName(String name);
	

	
}
