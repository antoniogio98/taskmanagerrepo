package it.uniroma3.siw.taskmanager.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Comment {
	
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
Long id;

private String text;

@ManyToOne
private User user;

@ManyToOne
private Task task;

public Comment() {}


public String getText() {
	return text;
}

public void setText(String text) {
	this.text = text;
}


public Long getId() {
	return id;
}


public void setId(Long id) {
	this.id = id;
}


public User getUser() {
	return user;
}


public void setUser(User user) {
	this.user = user;
}


public Task getTask() {
	return task;
}


public void setTask(Task task) {
	this.task = task;
}




}
