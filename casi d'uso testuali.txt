Descrizione testusale casi d'uso progetto taskmanager :
_______________________________________________________________________________________________________________
CASI D'USO USER

1) REGISRAZIONE: 
   Un utente si registra al sistema inserendo le proprie credenziali (l'username e la password) nome e cognome
 
2) LOGIN: 
   Un utente registrato effettua il login inserendo username e password

3) UPDATE DATI UTENTE:
   Un Utente che ha effettuato l'accesso usa il sistema per visualizzare i propri dati, il sistema mostra nome, cognome usename e le 
   date di creazione e ultimo update dell' utente. L'utente clicca su "update profile" , il sistema mostra la schermata che permette
   all'utente di modificare il nome e il cognome

4) CREAZIONE NUOVO PROGETTO: 
   Un Utente che ha effettuato l'accesso può usare il sistema per creare un nuovo progetto cliccando su "create new project",
   ogni progetto ha un nome e una descrizione

5) VISUALIZZARE LISTA PROGETTI DI CUI SI E' PROPRIETARI: 
   Un utente autenticato clicca su "my projects", il sistema mostra la vista con tutti i progetti di cui l'utente e proprietatio

6) UPDATE DATI PROGETTO: 
   All'interno della vista con tutti i progetti l'utente può accedere alla vista del singolo progetto da dove è possibile 
   fare l'update del progetto, il sistema mostra la vista per modificare nome e descrizione    

7) DELETE DI UN PROGETTO: 
   All'interno della vista con tutti i suoi progetti l'utente può eliminare un progetto, il sistema mostra nuovamente 
   la vista senza il progetto appena eliminato

8) CONDIVIDERE UN PROGETTO: 
   Un utente autenticato, proprietario di un progetto, puo usare il sistema per accedere alla vista del proprio progetto,
   l'utente clicca sul pulsante di share project, il sistema mostra la vista in cui inserire l'username dell'utente con il quale 
   si vuole condividere il progetto. 

9) VISUALIZZARE LISTA PROGETTI CONDIVISI:
   L'utente autenticato clicca su "shared with me" il sistema mostra la lista di progetti che gli altri utenti hanno condiviso 
   con l'utente corrente
   
9) CREARE UN TASK: 
   L' utente autenticato (dalla vista del progetto) può usare il sistema per aggiungere task al progetto di cui
   è il proprietario, il sistema mostra la vista del nuovo task. Ogni task ha un nome e una descrizione 

10) ASSEGNAZIONE TASK:
    L'utente che ha creato il task può assegnare il task a un utente che ha visibilità del progetto 
    cliccando su "assign to", il sistema mostra la vista in cui inserire l'username dell' utente al quale si vuole assegnare il task

11) ELIMINARE UN TASK:
    dalla vista del progetto, l'owner può eliminare i task realtivi a quel progetto

12) AGGIUNGRE TAG: 
    E' possibile aggiungre un tag ad ogni task di un determinato progetto. Il tag ha un nome un colore (stringa) e una descrizione

13) AGGIUNGRE COMMENTO AD UN TASK:
    L'utente che ha visibilità su un progetto (dalla vista del task) può aggiungere un commento al task corrente

14) UPDATE DATI TASK:
   All'interno della vista di un progetto l'utente può accedere alla lista dei task da dove è possibile 
   fare l'update del task selezionato, il sistema mostra la vista per modificare nome e descrizione      

_______________________________________________________________________________________________________________________________
CASI D'USO ADMIN

1) VISUALIZZARE LISTA UTENTI:
   L'admin del sistema clicca su "All user list" il sistema mostra la lista di tutti gli utenti registrati nel sistema 

2) ELIMINAZIONE DI UN UTENTE:
   L'admin del sistema, dalla vista della lista di tutti gli user, clicca sul pulsante "delete", il sistema elimina l'user con tutti 
   i progetti di cui è in possesso

3) ELIMINARE IL PROGETTO DI UN UTENTE:
   L'admin del sistema, dalla vista della lista di tutti gli user, clicca su uno user, il sistema mostra la vista dei 
   progetti dello user selezionato, cliccando "delete" può eliminare un progetto
